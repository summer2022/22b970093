## 安装

+ rpm -Uvh uutils-coreutils-0.0.14-1.x86_64.rpm --replacefiles （和coreutils冲突，且coreutils难卸载，故采用此方式）

```
[csmsoledad@localhost bin]$ ls -al | grep cat
-rwxr-xr-x  1 root root      166152 Sep 30  2021 bsdcat
-rwxr-xr-x  1 root root       95576 Sep 30  2021 bzcat
-rwxr-xr-x  1 root root     1198696 Jul 17 16:35 cat
```
